<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\index;
class IndexPageController extends Controller
{
    public function index() {
        return view('index');
    }
}